package com.RRG.airqualityindex.Model;

import com.RRG.airqualityindex.Model.POJO.AQI_data_search;
import com.RRG.airqualityindex.Model.POJO.AQI_data_singleStation;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Retrofit HTTP Get methods here.
 */
public interface AQI_api {

     String TOKEN = ""; //private token obtained though API web provider
     String BASE_URL = "https://api.waqi.info/";
    String BASE_URL_MAP = "https://tiles.aqicn.org/tiles/";

    @GET("search/")
    Call<AQI_data_search> getAQISearchData(@Query("token") String token, @Query("keyword") String keyword);

    @GET("feed/@{id}/")
    Call<AQI_data_singleStation> getAQIStation(@Path("id")int uid,@Query("token") String token);
}
