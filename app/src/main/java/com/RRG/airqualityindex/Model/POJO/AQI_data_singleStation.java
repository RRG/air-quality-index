package com.RRG.airqualityindex.Model.POJO;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AQI_data_singleStation {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private com.RRG.airqualityindex.Model.POJO.Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public com.RRG.airqualityindex.Model.POJO.Data getData() {
        return data;
    }

    public void setData(com.RRG.airqualityindex.Model.POJO.Data data) {
        this.data = data;
    }

}
