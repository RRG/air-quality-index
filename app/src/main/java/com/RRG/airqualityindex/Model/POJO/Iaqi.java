package com.RRG.airqualityindex.Model.POJO;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Iaqi {

    @SerializedName("dew")
    @Expose
    private dew dew;
    @SerializedName("h")
    @Expose
    private H h;
    @SerializedName("p")
    @Expose
    private P p;
    @SerializedName("pol")
    @Expose
    private Pol pol;
    @SerializedName("t")
    @Expose
    private T t;
    @SerializedName("w")
    @Expose
    private W w;
    @SerializedName("wg")
    @Expose
    private Wg wg;

    public dew getdew() {
        return dew;
    }

    public void setdew(dew dew) {
        this.dew = dew;
    }

    public H getH() {
        return h;
    }

    public void setH(H h) {
        this.h = h;
    }

    public P getP() {
        return p;
    }

    public void setP(P p) {
        this.p = p;
    }

    public Pol getPol() {
        return pol;
    }

    public void setPol(Pol pol) {
        this.pol = pol;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public W getW() {
        return w;
    }

    public void setW(W w) {
        this.w = w;
    }

    public Wg getWg() {
        return wg;
    }

    public void setWg(Wg wg) {
        this.wg = wg;
    }

}
