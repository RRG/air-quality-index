package com.RRG.airqualityindex.Model;

import androidx.annotation.NonNull;


/**
 * class that represent a "shortened" version of the data pulled
 * from the API (unnecessary data filtered out)
 */
public class AQI_data_compressed {
    private int uniqueID;
    private String aqi;
    private String time;
    private String stationName;


    public int getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(int uniqueID) {
        this.uniqueID = uniqueID;
    }

    public String getAqi() {
        return aqi;
    }

    public void setAqi(String aqi) {
        this.aqi = aqi;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    @NonNull
    @Override
    public String toString() {
        return uniqueID+"\n"+ aqi+"\n"+time+"\n"+stationName+"\n";
    }
}
