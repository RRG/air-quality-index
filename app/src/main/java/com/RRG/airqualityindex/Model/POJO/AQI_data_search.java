package com.RRG.airqualityindex.Model.POJO;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Java representation of JSON obtained from URL
 * for Retrofit to properly translate them into
 * Plain old java objects (POJO).
 */
public class AQI_data_search {

    public String status;
    public List<JsonList> data = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<JsonList> getData() {
        return data;
    }

    public void setData(List<JsonList> data) {
        this.data = data;
    }

    public class JsonList{

        @SerializedName("uid")
        public int uniqueId;

        public String aqi;

        public TimeData time = null;

        public int getUniqueId() {
            return uniqueId;
        }

        public void setUniqueId(int uniqueId) {
            this.uniqueId = uniqueId;
        }

        public String getAqi() {
            return aqi;
        }

        public void setAqi(String aqi) {
            this.aqi = aqi;
        }

        public TimeData getTime() {
            return time;
        }

        public void setTime(TimeData time) {
            this.time = time;
        }

        public StationData getStation() {
            return station;
        }

        public void setStation(StationData station) {
            this.station = station;
        }

        public class TimeData{

            @SerializedName("tz")
            public String timezone;

            @SerializedName("stime")
            public String standardTime;

            @SerializedName("vtime")
            public String virtualTime;

            public String getTimezone() {
                return timezone;
            }

            public void setTimezone(String timezone) {
                this.timezone = timezone;
            }

            public String getStandardTime() {
                return standardTime;
            }

            public void setStandardTime(String standardTime) {
                this.standardTime = standardTime;
            }

            public String getVirtualTime() {
                return virtualTime;
            }

            public void setVirtualTime(String virtualTime) {
                this.virtualTime = virtualTime;
            }
        }

        public StationData station = null;


        public class StationData{

            @SerializedName("name")
            public String stationName;

            @SerializedName("geo")
            public List<Double> geoLocation;

            public String url;

            public String country;

            public String getStationName() {
                return stationName;
            }

            public void setStationName(String stationName) {
                this.stationName = stationName;
            }

            public List<Double> getGeoLocation() {
                return geoLocation;
            }

            public void setGeoLocation(List<Double> geoLocation) {
                this.geoLocation = geoLocation;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }
        }
    }

}
