package com.RRG.airqualityindex.Model;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.RRG.airqualityindex.AQI_presenter;
import com.RRG.airqualityindex.Model.POJO.AQI_data_search;
import com.RRG.airqualityindex.Model.POJO.AQI_data_singleStation;
import com.RRG.airqualityindex.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * this class' purpose is as a data provider to the presenter
 * MVP architecture (model)
 */
public class AQI_model {

    private ArrayList<AQI_data_compressed> results;
    private AQI_presenter presenter;
    private AQI_api apiService;
    private static final String AQI_IDENTIFIER ="AQI";
    private static final String TIME_STAMP_IDENTIFIER ="TIME_STAMP";
    private static final String AQI_ID ="AQI_ID";
    private static final String LOCATION_IDENTIFIER ="LOCATION";
    private SharedPreferences saveData;

    public AQI_model(AQI_presenter presenter){

        this.presenter = presenter;
        this.apiService = ApiClient.getClient().create(AQI_api.class);
    }

    /**
     * method to get data from API when searching for stations
     * @param keyword search keyword
     */
    public void getAQI_data(String keyword)  {

        Call<AQI_data_search> call = apiService.getAQISearchData(AQI_api.TOKEN,keyword);

        call.enqueue(new Callback<AQI_data_search>() {
            @Override
            public void onResponse(Call<AQI_data_search> call, Response<AQI_data_search> response) {
                if(!response.isSuccessful()){
                    presenter.onUnsuccessfulResponse(response.code());
                }else{
                    results = new ArrayList<>();
                    assert response.body() != null;
                    for(AQI_data_search.JsonList i :response.body().data ){
                        AQI_data_compressed temp = new AQI_data_compressed();
                        temp.setAqi(i.aqi);
                        temp.setUniqueID(i.uniqueId);
                        temp.setTime(i.time.standardTime);
                        temp.setStationName(i.station.stationName);
                        results.add(temp);
                    }
                    presenter.onSearchFinished(results);
                }
            }
            @Override
            public void onFailure(Call<AQI_data_search> call, Throwable t) {
                //notify user
                presenter.onUnsuccessfulRequest(t.getMessage());
            }
        });

    }

    /**
     * method to get data of a particular station from API when clicking the refresh button
     * @param uid station id to get new data for
     */
    public void getAQI_RefreshData(String uid)  {
        Call<AQI_data_singleStation> call = apiService.getAQIStation(Integer.parseInt(uid),AQI_api.TOKEN);

        call.enqueue(new Callback<AQI_data_singleStation>() {
            @Override
            public void onResponse(Call<AQI_data_singleStation> call, Response<AQI_data_singleStation> response) {
                if(!response.isSuccessful()){
                    presenter.onUnsuccessfulResponse(response.code());
                }else{
                    assert response.body() != null;
                    AQI_data_compressed temp = new AQI_data_compressed();
                    temp.setAqi(response.body().getData().getAqi().toString());
                    temp.setUniqueID(response.body().getData().getIdx());
                    temp.setTime(response.body().getData().getTime().getS());
                    temp.setStationName(response.body().getData().getCity().getName());
                    presenter.onRefreshFinished(temp);
                }
            }

            @Override
            public void onFailure(Call<AQI_data_singleStation> call, Throwable t) {
                //notify user
                presenter.onUnsuccessfulRequest(t.getMessage());
            }
        });
    }

    /**
     * Method to save the AQI the user was last looking at
     * @param context main activity context
     * @param aqi  aqi data
     * @param timeStamp timeStamp data
     * @param location location data
     * @param id  AQI station ID
     */
    public void saveAQI_data(Context context, String aqi, String timeStamp, String location, int id){

        saveData  = context.getSharedPreferences("AQI_DATA", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = saveData.edit();
        editor.putString(AQI_IDENTIFIER, aqi);
        editor.putString(TIME_STAMP_IDENTIFIER, timeStamp);
        editor.putString(LOCATION_IDENTIFIER, location);
        editor.putInt(AQI_ID,id);
        editor.apply();
    }

    /**
     * Method to retrieve the AQI data the user was last looking at "if any"
     * @param context  main activity context
     * @return aqi data in an AQI_data_compressed object
     */
    public AQI_data_compressed getSavedAQI_data(Context context) {
        saveData  = context.getSharedPreferences("AQI_DATA", Context.MODE_PRIVATE);
        String aqi = saveData.getString(AQI_IDENTIFIER, context.getResources().getString(R.string.defaultAQI));
        String timeStamp = saveData.getString(TIME_STAMP_IDENTIFIER, context.getResources().getString(R.string.defaultAQI));
        String location = saveData.getString(LOCATION_IDENTIFIER, context.getResources().getString(R.string.defaultAQI));
        int id = saveData.getInt(AQI_ID, 0);
        AQI_data_compressed aqiData = new AQI_data_compressed();
        aqiData.setAqi(aqi);
        aqiData.setStationName(location);
        aqiData.setTime(timeStamp);
        aqiData.setUniqueID(id);
        return aqiData;
    }
}
