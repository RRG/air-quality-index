package com.RRG.airqualityindex;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.RRG.airqualityindex.Model.AQI_data_compressed;
import com.google.android.material.bottomappbar.BottomAppBarTopEdgeTreatment;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;


/**
 * this class' purpose is to only update views (UI) and
 * notify the presenter if the user performed an action
 * on the interface
 * MVP architecture (View)
 * -------------------------------
 */
public class MainActivity extends AppCompatActivity {

    private AQI_presenter presenter;
    //private ListView searchResults; //old
    private TextView aqi;
    private TextView healthiness;
    private TextView timeStamp;
    private TextView location;
    private TextInputEditText keyword;
    private PopupWindow aqi_info_popup;
    private ProgressBar progres_bar;
    private ObjectAnimator progressAnimator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main_new);

        //Toolbar toolbar = findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        //init every view needed
        presenter = new AQI_presenter(this);
        //searchResults = findViewById(R.id.lv_searchResult);
        aqi = findViewById(R.id.tv_aqi);
        healthiness = findViewById(R.id.tv_healthiness);
        timeStamp = findViewById(R.id.tv_timestamp);
        location = findViewById(R.id.tv_location);
        keyword = findViewById(R.id.et_keyword);
        progres_bar = findViewById(R.id.aqi_progress_bar);

/*  //old
        searchResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                presenter.setReadings(position);
            }
        });*/

        //try and get the last AQI the user was looking at and update to latest value, else will display default
        presenter.getPersistentData();
        presenter.getRefreshData( location.getTag().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.toolbar_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case R.id.mi_refresh:
                //update aqi
                if(!location.getTag().equals("0000")){ //avoid refreshing when no station has been chosen yet!
                   // Toast.makeText(this,location.getTag().toString(),Toast.LENGTH_LONG).show();
                    //Log.i("code",location.getTag().toString());
                    presenter.getRefreshData( location.getTag().toString());
                }
                break;
            case R.id.mi_aqi_info:
                //show aqi number chart
                createAQI_infoUI();
                break;
            case R.id.mi_website:
                //go to website
                Intent myIntent = new Intent(getApplicationContext(),Website.class);
                startActivity(myIntent);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public void on_refresh_clicked(View view){
        //update aqi
        if(!location.getTag().equals("0000")){ //avoid refreshing when no station has been chosen yet!
            // Toast.makeText(this,location.getTag().toString(),Toast.LENGTH_LONG).show();
            //Log.i("code",location.getTag().toString());
            presenter.getRefreshData( location.getTag().toString());
        }
    }
    public void on_info_clicked(View view){
        //show aqi number chart
        createAQI_infoUI();
    }
    public void on_web_clicked(View view){
        //go to website
        Intent myIntent = new Intent(getApplicationContext(),Website.class);

        Pair[] pairs = new Pair[1];
        pairs[0] = new Pair<View,String>(findViewById(R.id.btn_waqi_info), "web");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this,pairs);
            startActivity(myIntent,options.toBundle());
        }else {
            startActivity(myIntent);
        }
    }


    private void update_progress_bar(int value){
        progressAnimator = ObjectAnimator.ofInt(progres_bar, "progress", value);
        progressAnimator.setDuration(2000);
        progressAnimator.start();
    }


    public void onSearch(View view){
        presenter.getSearchData(keyword.getText().toString());
        //clear text from edit text when search has been initiated
        keyword.getText().clear();
        //clear keyboard from screen when button is pressed
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(keyword.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

    /**
     * method to populate search result list
     * @param data search result array
     */
    public void updateUI(ArrayList<String> data){
        ArrayAdapter adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, data);

//version 2
       //THIS IS AN ALTERNATE WAY OF PRESENTING THE SEARCH RESULTS USING ANDROID DIALOGS.
        // setup the alert builder
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this,
                R.style.AlertDialogTheme);
        final AlertDialog alert = alertDialog.create();
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.custom_dialog, null);
        alert.setView(convertView);
        alert.setTitle("Choose a station");
        ListView lv = (ListView) convertView.findViewById(R.id.lv_test_custom_dialog);
        lv.setAdapter(adapter);

        //list view click listener
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                alert.cancel();
                presenter.setReadings(position);
            }
        });
        // add Cancel button
        final Button cancel_button = convertView.findViewById(R.id.btn_cancel);
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        // animation work.
        if (alert.getWindow() != null) {
            alert.getWindow().getAttributes().windowAnimations = R.style.AlertDialogTheme;
        }
        alert.show();

//Version 1
/*
        searchResults.setAdapter(adapter);*/
    }

    /**
     * method to populate the AQI card
     * @param data AQI data
     * @param classification a pair which dictates the colour of the AQI depending on its value (computed in presenter)
     */
    public void updateAQI_UI(AQI_data_compressed data, Pair<Integer,String> classification) {
        //update aqi reading with appropriate colours
        aqi.setText(data.getAqi());
        update_progress_bar(Integer.parseInt(data.getAqi()));
        //programmatically overlapping background colour of shape attr with new one.
        aqi.getBackground().setColorFilter(getResources().getColor(classification.first), PorterDuff.Mode.SRC_ATOP);
        healthiness.setText(classification.second);
        healthiness.setTextColor(getResources().getColor(classification.first));
        timeStamp.setText(data.getTime());
        location.setText(data.getStationName());
        location.setTag(data.getUniqueID());

    }


    public void toastUI(String s) {
        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
    }

    /**
     * method to display that the searched station is unable to give a reading
     */
    public void stationDown() {
        healthiness.setText(R.string.stationError);
        healthiness.setTextColor(getResources().getColor(R.color.White));
        aqi.setText(R.string.defaultAQI);
        timeStamp.setText(R.string.defaultAQI);
        location.setText(R.string.defaultAQI);
        aqi.setBackground(ContextCompat.getDrawable(this, R.drawable.aqi_background));
        location.setTag(getString(R.string.defaultAQI));
    }

    /**
     * when clicking map button
     * @param view
     */
    public void onMapClick(View view){
        if(presenter.checkGooglePlayServices()){
            Intent myIntent = new Intent(getApplicationContext(),Maps.class);

            Pair[] pairs = new Pair[1];
            pairs[0] = new Pair<View,String>(findViewById(R.id.ib_viewMap), "map");

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this,pairs);
                myIntent.putExtra("URL",presenter.getTileURl());
                startActivity(myIntent,options.toBundle());
            }else {
                myIntent.putExtra("URL",presenter.getTileURl());
                startActivity(myIntent);
            }
        }
    }


    /**
     * This method creates the UI for the pop up view with the AQI info on it
     */
    public void createAQI_infoUI(){
        //show aqi number chart
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        // Inflate the custom layout/view
        View customView = inflater.inflate(R.layout.popup_aqi_info,null);

        // Initialize a new instance of popup window
        // focusable lets taps outside the popup also dismiss it
        aqi_info_popup = new PopupWindow(
                customView, CardView.LayoutParams.WRAP_CONTENT, CardView.LayoutParams.WRAP_CONTENT,true);

        if(Build.VERSION.SDK_INT>=21){
            aqi_info_popup.setElevation(5.0f);
        }

        // Get a reference for the custom view close button
        ImageButton closeButton = customView.findViewById(R.id.ib_close_popup);

        // Set a click listener for the popup window close button
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Dismiss the popup window
                aqi_info_popup.dismiss();
            }
        });
        aqi_info_popup.setAnimationStyle(R.style.popup_Animation);
        // show the popup window
        // which view you pass should usually be the custom view defined/inflated for the popup window
        aqi_info_popup.showAtLocation(customView, Gravity.CENTER, 0, 0);

    }

    public void onAttribution_clicked(View view){
        new MaterialAlertDialogBuilder(MainActivity.this, R.style.AlertDialogTheme)
                .setView((View) getLayoutInflater().inflate(R.layout.attribution, null))
                .setTitle("Attributions")
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    /**
     * This lifecycle is used to determine at which point to save current AQI data persistently
     */
    @Override
    public void onPause() {
        super.onPause();
        presenter.saveDataPersistently(aqi.getText().toString(),
                timeStamp.getText().toString(),
                location.getText().toString(),
                Integer.parseInt(location.getTag().toString()));
    }

    @Override
    public void onBackPressed() {
        //do not allow to go back to splash
    }
}
