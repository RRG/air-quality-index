package com.RRG.airqualityindex;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.util.Pair;

import com.RRG.airqualityindex.Model.AQI_api;
import com.RRG.airqualityindex.Model.AQI_model;
import com.RRG.airqualityindex.Model.AQI_data_compressed;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.ArrayList;

import static java.lang.Character.isDigit;

/**
 * this class' purpose is to serve as a middle man between the model
 * and the view. All data formatting before sending to the views are
 * done here
 * MVP architecture (presenter)
 */

public class AQI_presenter {

    private AQI_model model;
    private MainActivity view;
    private ArrayList<AQI_data_compressed> latestSearch;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    public AQI_presenter(MainActivity view){
        this.view = view;
        model = new AQI_model(this);
    }


    /**
     * method called by VIEW to pull data from web service (via presenter as
     * middle man)
     * @param keyword keyword searched
     */
    public void getSearchData(String keyword) {
        if (keyword.equals("")){
            view.toastUI("Type something !");
        }else {
            model.getAQI_data(keyword);
        }
    }

    /**
     * method called by VIEW upon clicking refresh button.
     * new (updated data) of current station will be pulled
     * from web service and displayed or in cases where stations do
     * not return any reading, a message saying so is presented to the user
     * @param uid unique ID of station
     */
    public void getRefreshData(String uid) {
        if(uid.equals(view.getString(R.string.defaultAQI))){
            view.toastUI("Station still unresponsive");
        }else{
            model.getAQI_RefreshData(uid);
        }
    }

    /**
     * method called by MODEL when it finished pulling data
     * (async) from the web service
     * @param results results from API
     */
    public void  onSearchFinished(ArrayList<AQI_data_compressed> results){
        latestSearch = results;
        ArrayList<String> listViewArray = new ArrayList<>();
        for(AQI_data_compressed i : results){
            listViewArray.add(stringCleaner(i.getStationName()));
        }
        view.updateUI(listViewArray);
    }

    /**
     * method called by MODEL when it finished pulling data
     * (async) from the web service
     * @param refreshedResult new (updated) data of currently displayed AQI
     */
    public void  onRefreshFinished(AQI_data_compressed refreshedResult){
        view.toastUI("Readings Refreshed !");
        view.updateAQI_UI(refreshedResult,getColourAndHazardnessScheme(Integer.parseInt(refreshedResult.getAqi())));
    }

    /**
     * MODEL class calls this upon receiving a bad response from API
     * @param code error code
     *       TO-DO :make different error msgs with code checking
     */
    public void  onUnsuccessfulResponse(int code){
        view.toastUI("Error in getting data. \n Please Try again later");
    }

    /**
     * MODEL class calls this upon having a problem with the request
     * @param message error message
     *  TO-DO :make different error msgs with msg checking
     */
    public void onUnsuccessfulRequest(String message) {
        view.toastUI("Error in sending a request, please contact the developer");
    }

    /**
     * VIEW calls this when the user selected a station from the search results
     * @param position position of station nin search list
     */
    public void setReadings(int position) {
        AQI_data_compressed target = latestSearch.get(position);
        target.setTime(target.getTime());
        target.setStationName(stringCleaner(target.getStationName()));
        if(validateAqiReading(target.getAqi())){
            view.updateAQI_UI(target,getColourAndHazardnessScheme(Integer.parseInt(target.getAqi())));
        }else{
            view.stationDown(); //show that station is down
        }
    }

    /**
     * Method to validate the AQI passing through the presenter
     * @param aqi aqi to validate
     * @return boolean, True if aqi is valid
     */
    private boolean validateAqiReading(String aqi){
        //sometimes a station might return a "-" as AQI reading
        char[] charsTemp = aqi.toCharArray();
        boolean validAQI = true;
        for(Character i: charsTemp){  //checking aqi validity
            if (!isDigit(i)){
                validAQI = false;
            }
        }
        return validAQI;
    }

    /**
     * malaysia string cleaner for malaysia only
     * @param input string to clean
     * @return cleaned string
     */
    private String stringCleaner(String input){
        return input.replace("kualalumpura; ","");
    }

    /**
     * method to categorize AQI levels by colour and description before
     * sending it over to the VIEW(MVP)
     * @param aqi air quality index
     * @return tuple of (colour, description)
     */
    private Pair<Integer,String> getColourAndHazardnessScheme(int aqi) {
        Pair<Integer,String> result = null;
        if(aqi>=0 && aqi<=50){
            result = new Pair<>(R.color.good,"Good");
        }else if(aqi>=51 && aqi<=100){
            result = new Pair<>(R.color.Moderate,"Moderate");
        }else if(aqi>=101 && aqi<=150){
            result = new Pair<>(R.color.Unhealthy_for_sensitiveGroups,"Unhealthy for Sensitive Groups");
        }else if(aqi>=151 && aqi<=200){
            result = new Pair<>(R.color.Unhealthy,"Unhealthy");
        }else if(aqi>=201 && aqi<=300){
            result = new Pair<>(R.color.Very_Unhealthy,"Very Unhealthy");
        }else if(aqi>300){
            result = new Pair<>(R.color.Hazardous,"Hazardous");
        }
        return result;
    }

    /**
     * method to obtain map tile URL (w/ format specifiers) & fill in known values
     * @return formatted URL
     */
    public String getTileURl(){
        return AQI_api.BASE_URL_MAP+"usepa-aqi/%d/%d/%d.png?token="+AQI_api.TOKEN;

    }

    /**
     * call to presenter when its time to save AQI data
     * @param aqi last AQI data the user was looking at
     * @param timeStamp  last AQI data the user was looking at
     * @param location last AQI data the user was looking at
     * @param id Id of station providing AQI reading
     */
    public void saveDataPersistently(String aqi, String timeStamp, String location,int id){
        model.saveAQI_data(view.getApplicationContext(),aqi,timeStamp,location,id);
    }

    /**
     * call to presenter when its time to fetch locally stored AQI data
     */
    public void getPersistentData() {
        AQI_data_compressed aqiData = model.getSavedAQI_data(view.getApplicationContext());
        if(aqiData != null){
            if(validateAqiReading(aqiData.getAqi())){
                view.updateAQI_UI(aqiData,getColourAndHazardnessScheme(Integer.parseInt(aqiData.getAqi())));
            }
        }
    }

    /**
     * checking if user has the appropriate google play services
     * @return true - yes, otherwise false with an error dialog.
     */
    public boolean checkGooglePlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(view);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(view, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                //Log.i("save", "This device is not supported.");
                view.finish();
            }
            return false;
        }
        return true;

    }

}
