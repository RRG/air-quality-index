package com.RRG.airqualityindex;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * View class of MVP architecture to display a website
 */
public class Website extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_website);



        //getSupportActionBar().setTitle(R.string.website_name);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        WebView wv = findViewById(R.id.wv_website);
        wv.setWebViewClient(new WebViewClient());
        wv.loadUrl("https://aqicn.org/mask/");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void onBackPressed(){
        super.onBackPressed();
    }
}
